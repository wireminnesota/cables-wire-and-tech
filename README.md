Minnesota Wire

1835 Energy Park Dr

St Paul, MN 55108

United States

(651)-642-1800

Elsewhere on the net: https://mnwire.com/

We have been engineering and manufacturing elegant solutions for complex custom cable assemblies since 1968. Minnesota Wire is one of a few facilities of its kind. We are a full-service development and manufacturing house for wire, cable and harness assemblies. We excel at solving unique and difficult electrical interconnect problems for the medical, defense, industrial, aerospace and other markets.

Minnesota Wire Is A Custom Design, Development And Manufacturing Company For Wire, Cable And Interconnect Assemblies.

Business Hours: Monday - Friday: 08:00 AM - 04:30 PM, Saturday - Sunday: Closed